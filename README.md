### 介绍
该工具用于将 Excel 的 Issue 内容批量导入至 Gitee 的企业中。

### 操作手册

运行方式

`go run main.go access_token namespace_path excel_path`

参数介绍
- access_token: [必填]Gitee API v5 的 access_token，可见文档：[Gitee Oauth 文档](https://gitee.com/api/v5/oauth_doc#/)，授权的账号需要有创建 {namespace_path} 企业/团队 Issue 权限；
- namespace_path：[必填]企业地址或团队地址，如：open_harmony，openharmony, openharmony-sig
- excel_path：[必填]Excel 文件的地址，可以是相对地址，也可以是绝对地址

导入成功的效果

![输入图片说明](https://images.gitee.com/uploads/images/2021/1117/120857_4e7ce08a_13510.png "屏幕截图.png")

### 环境要求和编译
go 版本要求：`go 1.15`

根据系统环境，编译二进制包
```ruby
go version
#=> go version go1.15.5 darwin/amd64

# OS: darwin, arch: amd64，可执行下面对应环境的命令编译二进制包
```

```
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o issue-importor-linux-amd64 main.go
CGO_ENABLED=0 GOOS=linux GOARCH=386 go build -o issue-importor-linux-386 main.go
CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -o issue-importor-mac main.go
CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o issue-importor-windows.exe main.go
```

### Excel 的模板格式

![输入图片说明](https://images.gitee.com/uploads/images/2021/1115/200852_e86d646e_5126245.png "屏幕截图.png") 

- 只导入可见的『工作表』，隐藏的工作表不导入；
- 第一行和第二行忽略，从第三行开始导入；
- 每一列说明：
  - A：编码，必填；
  - B：标题，必填；
  - C：描述，必填；
  - D：代码仓地址，必填。代码仓必须在{namespace_path}的企业/团队下，否则要停止导入，不带*.git* 后缀
  - E：编号，Issue 的 ident，导入成功后回填。如编号不为空，则此行跳过导入
- 导入的规则：
  - Issue的标题 = 编码：标题  
  - Issue的描述 = 描述
